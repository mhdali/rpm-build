FROM centos:6.8

RUN yum groupinstall -y "Development tools"
RUN yum install -y tar gcc bzip2 rpmdevtools rpmlint yum-utils

RUN cd /root && curl http://www.fefe.de/dietlibc/dietlibc-0.33.tar.bz2 | tar -jx
RUN cd /root/dietlibc-0.33 && make && install bin-*/diet /usr/local/bin

ADD checkinstall-1.6.2-20.4.x86_64.rpm /root
RUN rpm -i /root/checkinstall-*.rpm

