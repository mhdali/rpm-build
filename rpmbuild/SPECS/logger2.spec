Summary:     Logger 2.28
Name:           logger2
Version:        2.28
Release:        1
# License is a compulsory field so you have to put something there.
License:        GPLv2 and GPLv2+ and GPLv3+ and LGPLv2+ and BSD with advertising and Public Domain
##Source:         %{name}.tar.gz
# This package doesn't contain any binary files so it's architecture independent, hence
# specify noarch for the BuildArch.
##BuildArch:      noarch
##BuildRoot:      %{_tmppath}/%{name}-build
# I don't worry too much about the group since now one uses the rpm except me
# There's a list at /usr/share/doc/packages/rpm/GROUPS but you don't have to use one of them
# I just use System/Base by default and only change it if something more suitable occurs to me
##Group:          System/Base
##Vendor:         Your name here

%description
This is package contain only logger version 2.28

%prep
# the set up macro unpacks the source bundle and changes in to the represented by
# %{name} which in this case would be my_maintenance_scripts. So your source bundle
# needs to have a top level directory inside called my_maintenance _scripts
##%setup -n %{name}

%build
# this section is empty for this example as we're not actually building anything

%install
# create directories where the files will be located
mkdir -p $RPM_BUILD_ROOT/usr/bin
mkdir -p $RPM_BUILD_ROOT/usr/share/bash-completion/completions
mkdir -p $RPM_BUILD_ROOT/usr/share/man/man1


# put the files in to the relevant directories.
# the argument on -m is the permissions expressed as octal. (See chmod man page for details.)
install -m 755 logger2 $RPM_BUILD_ROOT/usr/bin
install -m 644 logger2.bash $RPM_BUILD_ROOT/usr/share/bash-completion/completions/logger2
install -m 644 logger2.1.gz $RPM_BUILD_ROOT/usr/share/man/man1

%post
# the post section is where you can run commands after the rpm is installed.
#insserv /etc/init.d/my_maintenance

%clean
rm -rf $RPM_BUILD_ROOT
rm -rf %{_tmppath}/%{name}
rm -rf %{_topdir}/BUILD/%{name}*

# list files owned by the package here
%files
%defattr(-,root,root)
/usr/bin/logger2
/usr/share/bash-completion/completions/logger2
/usr/share/man/man1/logger2.1.gz

%changelog
* Sun Aug 01 2010  Your name here
- 1.0 r1 First release
